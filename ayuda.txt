¡Generador Web Personal de Biblioteca Libre Autogestionada!
============================================

Este menú de ayuda pretende, aunque sea someramente, guiarte por 
el breve y sencillo proceso de generar tu listado de libros para 
compartir en una página web estática, anti obsolescente que pueda compartirse
en cualquier computadora.
¿Qué voy a necesitar?
Vamos a dividir este breve tutorial en 2:
1) Generación de la página web bibilioteca de lucha anti obsolescencia :)
2) ¡Plantar bandera con tus libros en internet!

Para el punto 1, vamos a necesitar
Tener instalado Python en la distro. Generalmente viene por defecto en 
casi todas las distribuciones.

Para el punto 2, o sea, donde vamos a ofrecer el sitio
a) Verificar que tenés salida a internet, o sea, que tu ip de internet
sea verdaderamente una ip pública y que esté en el rango de 100.x.x.x. 
Esto último querrá decir que tu proveedor de internet te tiene detrás de lo que se 
llama CG-NAT, o sea que usa un Switch donde divide la ip publica entre varios usuarios
https://www.cual-es-mi-ip.net/

Hay posibilidades de saltarse esto:
https://www.sdos.es/blog/ngrok-una-herramienta-con-la-que-hacer-publico-tu-localhost-de-forma-facil-y-rapida
Sin embargo, lo recomendable sería que busques un sitio que te permita subir tu web estática gratuitamente y tus libros
como podría ser:
https://www.netlify.com/


b) Tener instalado un servidor web, como puede ser Apache o Nginx
En esa PC ejecutamos
sudo apt install nginx

