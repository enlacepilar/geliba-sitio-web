# GeLiBA Sitio web

## Generador Libre de Bibliotecas Automatizadas 


## ¿Qué es?
Con una aplicación de escritorio hecha en Python con Tkinter, la idea del proyecto es generar, con unos simples pasos, el listado de la carpeta con tu biblioteca digital (en PDF, Epub, etc) y compartirla en una página web estática, anti obsolescente, es decir,  que se pueda “levantar” en cualquier computadora con un servidor web instalado como Apache, Nginx, Hiawatha, etc.

## ¿Por qué autogestionada? 
Porque el proyecto está a favor de una computación de bajo consumo, ya que podría desplegarse en una simple Raspberry Pi. Si no se cuenta con un equipo de este estilo puede ser un CPU, si bien no de bajo consumo, de los considerados “obsoletos”, como un Athlon 64.


## ¿A quién va dirigido?
Está dirigido principalmente a usuarios que carecen de conocimientos técnicos pero que quisieran poder ofrecer su biblioteca con libros digitales en internet para compartir y descargar libremente.


## ¿Dónde se encuentra alojada esta web?

Este sitio está alojado en una Raspberry Pi 4, una microcomputadora hogareña, favoreciendo así el autohosteo, también pilar no menos importante que tiene que ver con todo lo que pregona el proyecto de GeLiBA, es decir, ser anti obsolescente, escapar de las grandes infraestructuras y abogar por un internet de bajo consumo y baja tecnología.

El dominio principal es [https://geliba.enlacepilar.com.ar/](https://geliba.enlacepilar.com.ar/), pero si por la razón que fuere este no se encontrara funcionando (puede pasar por diversos motivos), existe un espejo creado para razones extraoridnarias al que se puede acceder de la siguiente manera:

[https://geliba.netlify.app/](https://geliba.netlify.app/)

